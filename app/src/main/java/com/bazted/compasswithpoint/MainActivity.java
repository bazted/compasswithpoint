package com.bazted.compasswithpoint;

import android.Manifest;
import android.animation.ValueAnimator;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.TimeUnit;

import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks,
        SensorEventListener {
    private static final String LOCATION_PERMISSION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String LOCATION_FINE_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;

    private static final int RC_LOCATION = 119;


    private ImageView compassBg;
    private ImageView compassBearing;

    private static final long METERS_BETWEEN_LOCATIONS = 2;

    private static final long MILLIS_BETWEEN_LOCATIONS = TimeUnit.SECONDS.toMillis(3);


    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Sensor magnetometer;


    private LocationManager locationManager;
    private Location mLocation;
    private float[] gravity;
    private float[] geomagnetic;
    private GeomagneticField geomagneticField;
    private float bearingLatitude = 0f;
    private float bearingLongitude = 0f;
    private ValueAnimator animator;
    private float north;
    private LocationListener locationListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        compassBg = (ImageView) findViewById(R.id.compassBg);
        compassBearing = (ImageView) findViewById(R.id.compassBearing);

        final EditText latitudeEt = (EditText) findViewById(R.id.latitudeEt);
        final EditText longitudeEt = (EditText) findViewById(R.id.longitudeEt);
        final FloatingActionButton button = (FloatingActionButton) findViewById(R.id.navBtn);

        //btn should be found otherwise throw NPE
        //noinspection ConstantConditions
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    bearingLatitude = Float.parseFloat(latitudeEt.getText().toString());
                    bearingLongitude = Float.parseFloat(longitudeEt.getText().toString());
                    compassBearing.setVisibility(View.VISIBLE);

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    compassBearing.setVisibility(View.INVISIBLE);
                    Toast.makeText(MainActivity.this, "Wrong bearing coordinates", Toast.LENGTH_SHORT).show();
                }
            }
        });

        this.sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        this.locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        prepareAnimator();

    }

    private void prepareAnimator() {
        animator = new ValueAnimator();
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(200);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                north = (float) animation.getAnimatedValue();
                compassBg.setRotation(north);
                compassBearing.setRotation(north + getBearingDegrees());
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
        prepareLocationManager();
    }

    @SuppressWarnings("MissingPermission")//checked via EasyPermissions
    public void prepareLocationManager() {
        if (!EasyPermissions.hasPermissions(this, LOCATION_FINE_PERMISSION, LOCATION_PERMISSION)) {
            askForPerms();
            return;
        }
        Location lastLocation = locationManager
                .getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        if (lastLocation != null) {
            mLocation = lastLocation;
            refreshGeomagneticField();
        }

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mLocation = location;
                refreshGeomagneticField();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                MILLIS_BETWEEN_LOCATIONS, METERS_BETWEEN_LOCATIONS, locationListener, Looper.getMainLooper());
    }

    private void refreshGeomagneticField() {
        geomagneticField = new GeomagneticField((float) mLocation.getLatitude(),
                (float) mLocation.getLongitude(), (float) mLocation.getAltitude(),
                mLocation.getTime());
    }


    @SuppressWarnings("MissingPermission")//checked via EasyPermissions
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
        if (!EasyPermissions.hasPermissions(this, LOCATION_FINE_PERMISSION, LOCATION_PERMISSION)) {
            return;
        }
        locationManager.removeUpdates(locationListener);
    }

    private void askForPerms() {
        // Do not have permissions, request them now
        EasyPermissions.requestPermissions(this, "We need to get your location for bearing functionality",
                RC_LOCATION, LOCATION_FINE_PERMISSION, LOCATION_PERMISSION);
    }

    @SuppressWarnings("MissingPermission")
    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (perms.contains(LOCATION_FINE_PERMISSION) && perms.contains(LOCATION_PERMISSION)) {
            if (locationListener != null) {
                locationManager.removeUpdates(locationListener);
            } else {
                prepareLocationManager();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Toast.makeText(MainActivity.this, "No required perms", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            gravity = event.values;
        }
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            geomagnetic = event.values;
        }

        if (gravity != null && geomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, gravity, geomagnetic);
            if (success) {
                final float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                final float azimuth = orientation[0];
                double degree = -azimuth * 360f / (2f * Math.PI);

                if (geomagneticField != null) {
                    degree += geomagneticField.getDeclination();
                }

                animateTo(Double.valueOf(degree).floatValue());
            }
        }


    }

    private float getBearingDegrees() {
        float bearingDegrees = 0f;
        if (mLocation != null) {
            Location destLocation = new Location(mLocation);
            destLocation.setLatitude(bearingLatitude);
            destLocation.setLongitude(bearingLongitude);
            bearingDegrees = mLocation.bearingTo(destLocation);
        }
        return bearingDegrees;
    }


    private void animateTo(float end) {
        if (!animator.isRunning()) {
            float start = north;
            float distance = Math.abs(end - start);
            float reverseDistance = 360.0f - distance;
            float dest;

            if (distance < reverseDistance) {
                dest = end;
            } else if (end < start) {
                dest = end + 360.0f;
            } else {
                dest = end - 360.0f;
            }

            animator.setFloatValues(start, dest);
            animator.start();

        }
    }

}
